#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <semaphore.h>

//Definiciones
#define NHILOSP 3
#define NHILOSC 3
#define N 3


//Declaracion de variables
sem_t mutex,vacio,lleno;
int indice=0;
int buffer[N];
int totalproductor=0;
int totalconsumidor=0;



//Declaracion de funciones
void *productor();
void *consumidor();

int main()
{
	extern int totalconsumidor,totalproductor;
    pthread_t Pproductor[NHILOSP];
    pthread_t Pconsumidor[NHILOSC];
    int status, i;


    //Inicializacion de Semaforos
	sem_init (&mutex, 0,1);
	sem_init (&lleno, 0,0);
	sem_init (&vacio, 0,N);
 
    
    srand (time(NULL));

  	
    // Create NHILOS threads
    for (i = 1; i <= NHILOSP; i++) {
		if ((status = pthread_create(&Pproductor[i], NULL, productor,NULL)))
	    	exit(status);
		
    }

    for (i = 1; i <= NHILOSC; i++) {
		if ((status = pthread_create(&Pconsumidor[i], NULL, consumidor,NULL)))
	    	exit(status);
		
    }

    

    // Wait threads
    for (i = 1; i <= NHILOSC; i++) {
		pthread_join(Pconsumidor[i],NULL);

		
    }

    for (i = 1; i <= NHILOSP; i++) {
		pthread_join(Pproductor[i],NULL);
    }

    // Final result
    if(totalproductor==totalconsumidor)
    	printf("Funciona correctamente\n");

    return 0;
}



void *productor()
{
	
    extern int buffer[N];
    extern int indice,totalproductor;
    
    int numero;

    while(indice!=N)
    {
  
    	numero=rand()%1000;
	    sem_wait(&vacio);
	  	sem_wait(&mutex);
		//--------------------
		// SECCI�N CR�TICA
		//--------------------
		buffer[indice]=numero;
		indice++;	
		totalproductor=totalproductor+numero;
		sem_post (&mutex);
		sem_post (&lleno);
		// Secci�n residual

    }
    

	pthread_exit(NULL);
	
    
}



void *consumidor()
{
	
    extern int buffer[N];
    extern int indice,totalconsumidor;
    int numero;


    while(indice!=-1)
    {
    	
    	sem_wait(&lleno);
	  	sem_wait(&mutex);
		//--------------------
		// SECCI�N CR�TICA
		//--------------------
		numero=buffer[indice];
		indice--;	
		totalconsumidor=totalconsumidor+numero;
		sem_post (&mutex);
		sem_post (&vacio);
		// Secci�n residual
    }

   

	pthread_exit(NULL);
	
    
}